package models;
import java.util.Date;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


public class Order {
     int id; // id của order
     String customerName; //tên khách hàng
     long price; //tổng giá tiền   
     Date orderDate; //ngày thực hiện order
     boolean confirm; //đã xác nhận hay chưa
     String[] items; //danh sách mặt hàng đã mua
     

     //khởi tạo một tham số customerName
     public Order(String customerName) {
          this.id = 1;
          this.customerName = customerName;
          this.price = 120000;
          this.orderDate = new Date();
          this.confirm = true;  
          this.items = new String[] {"Book", "pen", "rule"};
     }

     //khởi tạo với tất cả tham số 
     public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
          this.id = id;
          this.customerName = customerName;
          this.price = price;
          this.orderDate = orderDate;
          this.confirm = true;
          this.items = new String[] {"Book", "pen", "rule"};
     }

     //khởi tạo không tham số
     public Order() {
          this("2");
     }

     //khởi tạo với 3 tham số 
     public Order(int id, String customerName, long price) {
          this(id, customerName, price, new Date(), true, new String[] {"Trung", "Ca", "Dau", "Rau"});
     }

     @Override
     public String toString() {
          //định dạng tiêu chuẩn Việt Nam
          Locale.setDefault(new Locale("vi", "VN"));
          //Định dạng cho ngày tháng 
           String pattern = "dd/mm/yy hh:mm:ss.SSS";
          DateTimeFormatter defaulTimeFormatter = DateTimeFormatter.ofPattern(pattern);
          //định dạng cho giá tiền 
          Locale usLocale = Locale.getDefault();
          NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
          //return trả ra chuỗi String
          return
          "Order[id = " + id
           + ", customerName = "
            + customerName + ", price = " + usNumberFormat.format(price) 
            +", orderDate = " +defaulTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
            + ", confirm: " + confirm + ",items = " + Arrays.toString(items) + "]";
            
     }
     
     
}
