import java.util.ArrayList;
import java.util.Date;
import models.Order; //import class order

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> arrayList = new ArrayList<>();
        //khởi tạo order với các tham số khác nhau
        Order order1 = new Order();
        Order order2 = new Order("Lan");
        Order order3 = new Order(3, "Long", 80000);
        Order order4 = new Order(4, "Nam", 75000, new Date(), false, new String[] {"hop mau", "tay", "giay mau"});
        //thêm object order vào danh sách
        arrayList.add(order1);
        arrayList.add(order2);
        arrayList.add(order3);
        arrayList.add(order4);

        //in ra màn hình
        for(Order order : arrayList){
            System.out.println(order.toString());
        }
    }
}
